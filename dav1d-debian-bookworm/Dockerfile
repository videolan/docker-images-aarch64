FROM arm64v8/debian:bookworm-20250203-slim

MAINTAINER VideoLAN roots <roots@videolan.org>

ENV IMAGE_DATE=202310122300

# If someone wants to use VideoLAN docker images on a local machine and does
# not want to be disturbed by the videolan user, we should not take an uid/gid
# in the user range of main distributions, which means:
# - Debian based: <1000
# - RPM based: <500 (CentOS, RedHat, etc.)
ARG VIDEOLAN_UID=499

RUN addgroup --quiet --gid ${VIDEOLAN_UID} videolan && \
    adduser --quiet --uid ${VIDEOLAN_UID} --ingroup videolan videolan && \
    echo "videolan:videolan" | chpasswd && \
    apt-get update && \
    apt-get -y dist-upgrade && \
    apt-get install -y --no-install-suggests --no-install-recommends \
        ca-certificates curl git build-essential clang meson ninja-build libtinfo5 zstd && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*

# install clang-5 from llvm.org binary release, requires libtinfo5
RUN curl https://releases.llvm.org/5.0.1/clang+llvm-5.0.1-aarch64-linux-gnu.tar.xz | tar xJ -C /opt

ENV PATH="$PATH:/opt/clang+llvm-5.0.1-aarch64-linux-gnu/bin"

USER videolan
