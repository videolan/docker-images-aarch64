FROM ubuntu:focal-20210416

ENV IMAGE_DATE=202110201700

# If someone wants to use VideoLAN docker images on a local machine and does
# not want to be disturbed by the videolan user, we should not take an uid/gid
# in the user range of main distributions, which means:
# - Debian based: <1000
# - RPM based: <500 (CentOS, RedHat, etc.)
ARG VIDEOLAN_UID=499

ENV SHADERC_BRANCH=v2021.0 \
    GLSLANG_BRANCH=11.4.0 \
    SPIRV_TOOLS_BRANCH=v2021.1 \
    SPIRV_HEADERS_COMMIT=dafead1765f6c1a5f9f8a76387dcb2abe4e54acd

RUN set -x && \
    ln -fs /usr/share/zoneinfo/UTC /etc/localtime && \
    addgroup --quiet --gid ${VIDEOLAN_UID} videolan && \
    adduser --quiet --uid ${VIDEOLAN_UID} --ingroup videolan videolan && \
    usermod --append --groups video videolan && \
    echo "videolan:videolan" | chpasswd && \
    apt-get update && \
    apt-get install --no-install-suggests --no-install-recommends -y \
        apt-utils git-core build-essential gnupg clang clang-tools gdb lcov \
        python python3-pip python3-setuptools python3-wheel python3-mako \
        libepoxy-dev liblcms2-dev libavutil-dev libavcodec-dev libavdevice-dev \
        libavfilter-dev libavformat-dev curl \
        mesa-vulkan-drivers libvulkan-dev vulkan-validationlayers vulkan-tools \
        cmake pkg-config ninja-build && \
    pip3 install meson gcovr && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/* && \
    cd /root && \
    git clone --depth 1 -b ${SHADERC_BRANCH}         https://github.com/google/shaderc && \
    cd shaderc && \
    git clone --depth 1 -b ${GLSLANG_BRANCH}         https://github.com/KhronosGroup/glslang.git         third_party/glslang && \
    git clone --depth 1 -b ${SPIRV_TOOLS_BRANCH}     https://github.com/KhronosGroup/SPIRV-Tools.git     third_party/spirv-tools && \
    git clone --single-branch                        https://github.com/KhronosGroup/SPIRV-Headers.git   third_party/spirv-headers && \
    cd third_party/spirv-headers && git checkout ${SPIRV_HEADERS_COMMIT} && cd ../.. && \
    mkdir build && cd build && \
    cmake -GNinja \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DSHADERC_SKIP_TESTS=1 \
        .. && \
    ninja && \
    cp -a libshaderc*/libshaderc*.a   /usr/local/lib && \
    cp -a libshaderc*/libshaderc*.so* /usr/local/lib && \
    cp -a ../libshaderc*/include /usr/local && \
    mkdir -p /usr/local/lib/pkgconfig && \
    cp -a shaderc*.pc /usr/local/lib/pkgconfig && \
    ldconfig && \
    cd /root && \
    rm -rf shaderc

USER videolan
